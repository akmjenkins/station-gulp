			<footer>
			
				<div class="sw">
					<div class="footer-row">
					
						<div class="footer-contact">
							<address>874 Topsail Road, St. John's, NL, A1N 3J9</address>	
							<span>709 74 0555</span>
							<span>709 7540323</span>
						</div><!-- .footer-contact -->
						
						<div class="footer-nav">
							<ul>
								<li><a href="#">About the Station</a></li>
								<li><a href="#">Sitemap</a></li>
								<li><a href="#">Legal</a></li>
								<li><a href="#">Contact Us</a></li>
							</ul>
						</div><!-- .footer-nav -->
						
						<div class="footer-social">
							<?php include('i-social.php'); ?>
						</div><!-- .footer-social -->
					
					</div><!-- .footer-row -->
				</div><!-- .sw -->
				
				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">The Station</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/dist/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
				
			</footer><!-- .footer -->

		</div><!-- .page-wrapper -->

		<script>
			var templateJS = {
				templateURL: 'http://dev.me/station',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>

		<script src="../assets/dist/js/main.gulp.js"></script>
	</body>
</html>