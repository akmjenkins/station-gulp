<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<div class="nav">
	<div class="sw full">
		
		<nav>
			<ul>
				<li><a href="#">Trainers</a></li>
				<li><a href="#">Programs</a></li>
				<li><a href="#">Transformations</a></li>
				<li><a href="#">Book Appointment</a></li>
				<li><a href="#">Blog</a></li>
			</ul>
		</nav>
		
		<div class="nav-meta">
			
			<form action="/" class="nav-search-form single-form">
				<div class="fieldset">
					<input type="text" name="s" placeholder="Search The Station...">
					<button class="t-fa-abs fa-search">Search</button>
					<button class="t-fa-abs fa-close toggle-search" type="button">Close</button>
				</div><!-- .fieldset -->
			</form><!-- .search-form -->
			
			<button class="t-fa-abs fa-search toggle-search" type="button">Search</button>
			<span class="t-fa-abs fa-phone tooltip" data-destroy-at="700" data-restoration="none" data-content="709-999-9999">709-999-9999</span>
			
		</div><!-- .nav-meta -->
	</div><!-- .sw -->
	
</div><!-- .nav -->